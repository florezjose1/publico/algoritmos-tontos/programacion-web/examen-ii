from django import template
from django.db.models import Q
from django.utils.timezone import timedelta
from datetime import datetime


register = template.Library()


@register.filter(is_safe=True)
def get_name(user):
    """Return name user"""
    name = ''
    if user.first_name:
        name += user.first_name
    if user.last_name:
        name += f" {user.last_name}"

    if not name and user.username:
        name = user.username

    if not name and user.email:
        name = user.email

    return name
