# Examen II - Programación WEB

Make sure Python 3.8 or higher is installed on your system.


###### Version django: `4.0.4`

### Description problem:
`/media/previo2-12022.pdf`


#### Clone Project
```
git clone 
```

#### Project setup
```
$ cd examen_ii
$ virtualenv -p python3 env
$ source env/bin/activate
```

#### Install requirements
```
pip3 install --no-deps -r requirements.txt
```

#### Run 
```
python3 manage.py runserver
```

#### Mount DataBase
1. Run migrations
```
python3 manage.py migrate
```

#### Create super User
```
python3 manage.py cratesuperuser
```

## =====================================

### Configuration menu
```
/admin
: username
: password
```

### New Users
```
/admin/authentication/userinformation/
```

### Settings menu
```
/admin/authentication/optionmenu/
```

### Settings role
```
/admin/authentication/rol/
```

##***************************************************
## DEMO IMAGES


#### HOME 
![Home](./media/home.png)



#### LOGIN 
![Login](./media/login.png)



#### SESSION 
![Home](./media/session.png)



#### ADMIN 
![Home](./media/admin.png)



Made with ♥ by [Jose Florez](www.joseflorez.co)
