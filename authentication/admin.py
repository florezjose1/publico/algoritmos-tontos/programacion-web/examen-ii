from django.contrib import admin

from authentication.models import Rol, OptionMenu, UserInformation


@admin.register(Rol)
class RolAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)


@admin.register(OptionMenu)
class OptionMenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'icono', 'role')


@admin.register(UserInformation)
class UserInformationAdmin(admin.ModelAdmin):
    list_display = ('user', 'identification', 'type_identification', 'cellphone', 'role')
