from django.contrib.auth.views import LogoutView
from django.urls import path, include

from authentication import views
from examen_ii import settings

app_name = 'authentication'
urlpatterns = [
    path('login/', views.SignInView.as_view(), name='login'),
    path('list-menu/', views.OptionMenuView.as_view(), name='list_menu'),
    path('logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('redirect-session', views.redirect_session, name='redirect_session'),
]
