from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.models import User
from django.db import models


class Rol(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = "rol"
        verbose_name = "Rol"
        verbose_name_plural = "Roles"


class OptionMenu(models.Model):
    name = models.CharField(max_length=50)
    url = models.URLField()
    icono = models.CharField(
        max_length=100,
        help_text='Copia el nombre de la clase que contiene el icon fontawesome.com'
    )
    role = models.ForeignKey(Rol, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "menu"
        verbose_name = "Menu"
        verbose_name_plural = "Menu"


class UserInformation(models.Model):
    CC = "cc"
    CE = "ce"
    WRITER = "writer"

    TYPE_IDENTIFICATION = [
        (CC, "Cédula ciudadanía"),
        (CE, "Cédula de extrangería"),
    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    identification = models.CharField(
        "Identificación", max_length=255, blank=True, null=True
    )
    type_identification = models.CharField(
        "Tipo Identificación", max_length=255, blank=True, null=True, choices=TYPE_IDENTIFICATION
    )
    cellphone = models.CharField("Teléfono", max_length=255)
    role = models.ForeignKey(Rol, on_delete=models.CASCADE)

    USERNAME_FIELD = "email"

    objects = UserManager()

    class Meta:
        db_table = "user"
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def __str__(self):
        return f"{self.id} | {self.user.first_name} {self.user.last_name}"

    @property
    def full_name(self):
        return f"{self.user.first_name} {self.user.last_name}"
