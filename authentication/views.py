from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View

from authentication.models import UserInformation, OptionMenu


def redirect_session(request):
    if request.user.is_authenticated:
        return redirect(reverse_lazy('authentication:list_menu'))

    return redirect(reverse_lazy('authentication:login'))


class SignInView(View):
    template_name = "authentication/login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse_lazy('authentication:redirect_session'))
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        user = User.objects.filter(email__exact=email).first()
        if user:
            user = authenticate(request, username=user.username, password=password)
            print(user)
            if user is not None:
                login(request, user)
                return redirect(reverse_lazy('authentication:redirect_session'))

        messages.warning(request, 'Usuario o contraseña incorrecto. Por favor intentalo de nuevo')
        return redirect(reverse_lazy('authentication:login'))


class OptionMenuView(LoginRequiredMixin, View):
    template_name = "authentication/list_menu.html"

    def get(self, request, *args, **kwargs):
        user = UserInformation.objects.get(id=request.user.id)
        menu = OptionMenu.objects.filter(
            role=user.role
        )
        return render(request, self.template_name, {'menu': menu, 'user': user})