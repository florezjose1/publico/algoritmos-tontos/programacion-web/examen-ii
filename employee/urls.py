from django.urls import path

from employee import views

app_name = 'employee'
urlpatterns = [
    path('/employees', views.ListEmployeeView.as_view(), name='list_employee'),
    path('<str:action>', views.ActionEmployeeView.as_view(), name='action_employee'),
]
