from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View

from employee.models import Employee


class ListEmployeeView(View):
    template_name = 'employee/list_employee.html'

    def get(self, request, *args, **kwargs):
        employees = Employee.objects.all()
        return render(request, self.template_name, {'employees': employees})


class ActionEmployeeView(View):
    template_name = 'employee/action_employee.html'

    def get(self, request, *args, **kwargs):
        try:
            action = kwargs.get('action')
            if action == 'edit':
                employee = Employee.objects.get(id=request.GET.get('id'))
                return render(request, self.template_name, {
                    'positions': Employee.POSITIONS_EMP,
                    'employee': employee,
                })
        except Exception as e:
            messages.error(request, "Estamos presentando un error, intentalo de nuevo.")
            return redirect(reverse_lazy('employee:list_employee'))
        return render(request, self.template_name, {'positions': Employee.POSITIONS_EMP})

    def get_data(self, data):
        name = data.get('name')
        position = data.get('position')
        years_old = data.get('years_old')
        return name, position, years_old

    def create(self, data):
        name, position, years_old = self.get_data(data)

        Employee.objects.create(
            name=name, position=position, years_old=years_old
        )

    def update(self, emp, data):
        name, position, years_old = self.get_data(data)
        if emp.name != name:
            emp.name = name
        if emp.position != position:
            emp.position = position
        if emp.years_old != years_old:
            emp.years_old = years_old

        emp.save()

    def post(self, request, *args, **kwargs):
        try:
            data = request.POST
            _id = request.GET.get('id')
            if _id:
                emp = Employee.objects.get(id=_id)
                if data.get('_delete', None):
                    emp.delete()
                    messages.success(request, "Empleado eliminado correctamente")
                    return redirect(reverse_lazy('employee:list_employee'))
                else:
                    self.update(emp, data)
                    messages.info(request, "Empleado actualizado correctamente")
                    return redirect(reverse_lazy('employee:list_employee'))
            else:
                self.create(data)
                messages.success(request, "Empleado creado correctamente")

            if data.get('_addanother', None):
                return redirect(reverse_lazy('employee:action_employee', kwargs={'action': 'add'}))
            elif data.get('_save', None):
                return redirect(reverse_lazy('employee:list_employee'))
        except Exception as e:
            messages.error(request, "Tenemos un error a la hora de registrar el empleado")
        return redirect(reverse_lazy('employee:list_employee'))
