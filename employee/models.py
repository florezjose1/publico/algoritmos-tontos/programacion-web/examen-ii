from django.db import models


class Employee(models.Model):
    MANAGER = "manager"
    SUB_MANAGER = "sub_manager"
    TECHNICAL_LEADER = "technical_leader"
    PROFESSIONAL = "professional"
    OPERATOR = "operator"
    VARIUS = "various_services"
    POSITIONS_EMP = [
        (MANAGER, "Gerente"),
        (SUB_MANAGER, "Sub Gerente"),
        (TECHNICAL_LEADER, "Lider tecnico"),
        (PROFESSIONAL, "Profesional"),
        (OPERATOR, "Operador"),
        (VARIUS, "Servicios varios")
    ]

    name = models.CharField(max_length=30)
    position = models.CharField(
        max_length=20,
        choices=POSITIONS_EMP
    )
    years_old = models.IntegerField("Edad")

    class Meta:
        db_table = "employees"
